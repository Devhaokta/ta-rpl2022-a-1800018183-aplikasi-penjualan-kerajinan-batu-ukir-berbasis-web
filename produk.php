<?php  
error_reporting(0);
require_once 'config/config.php';
require_once 'config/koneksi.php';

$limit = 5;
if(isset($_GET['p']))
{
    $noPage = $_GET['p'];
}
else $noPage = 1;

$offset = ($noPage - 1) * $limit;

$sqlIndex = "SELECT
produk.id_produk,
produk.Nama,
produk.gambar,
kategori.nama_kategori,
produk.harga,
produk.qty,
produk.deskripsi
FROM 
produk
INNER JOIN kategori on produk.nama_kategori = kategori.nama_kategori
WHERE kategori.nama_kategori = 'Relief'
ORDER BY
produk.id_produk DESC
LIMIT ".$mysqli->real_escape_string($offset).",". $limit;

//data untuk dihitung
$sql_rec = "SELECT id_produk FROM produk";

$total_rec = $mysqli->query($sql_rec);

//Menghitung data yang diambil
$total_rec_num = $total_rec->num_rows;

$qryIndex = $mysqli->query($sqlIndex);

//Total semua data
$total_page = ceil($total_rec_num/$limit);
?>

<!-- produk -->
    <section class="relief" id="relief">
        <div class="container produk">
            <?php while ($index = $qryIndex->fetch_assoc()) { ?>
                <div class="list-produk">
                        <img src="<?php echo $base_url."image/gambar produk/".$index['gambar']; ?>" class="wow fadeIn">
                            <br><br><h5 class="text-center"><?php echo $index['Nama']; ?></h5>
                            <br><p class="text-center" style="color: black; font-size: 0.6em"><?php echo $index['deskripsi']; ?></p>
                            <br><h5>Rp. <?php echo number_format($index['harga'],0,',','.'); ?></h5>
                        <?php if ($index['qty'] > 0) {?>
                            <a href="cart.php?halaman=cart&id_produk=<?php echo $index['id_produk'];?>&aksi=tambah_produk&jumlah=1" class="tombol tombol-beli">Beli Sekarang</a>
                       <?php } else { ?>    
                            <a style="pointer-events: none; cursor: default;background: grey;" href="cart.php?halaman=cart&id_produk=<?php echo $index['id_produk'];?>&aksi=tambah_produk&jumlah=1" class="tombol tombol-beli">Beli Sekarang</a>
                        <?php } ?>
                </div> 
            <?php } ?>
        </div>
    </section>
    <!-- end produk -->
