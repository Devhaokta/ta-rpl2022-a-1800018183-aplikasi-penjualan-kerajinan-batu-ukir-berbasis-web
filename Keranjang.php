<?php
    if (isset($_GET['id_produk'])) {
        $id_produk=$_GET['id_produk'];
        $sql= "SELECT * FROM produk WHERE id_produk='$id_produk'";
        $query = mysqli_query($connect, $sql);
        $data = mysqli_fetch_array($query);
        $id_produk=$data['id_produk'];
        $nama_produk=$data['Nama'];
        $gambar=$data['gambar'];
        $nama_kategori=$data['nama_kategori'];
        $harga=$data['harga'];
        $qty=$data['qty'];
        $id_pembeli = $_SESSION['id_pembeli'];

            
        $cek = mysqli_query($connect, "SELECT * FROM cart WHERE id_produk = '$id_produk' AND id_pembeli = '$id_pembeli'");
        $ketemu = mysqli_num_rows($cek);
        if ($ketemu==0 && $qty > 0) {
            $jumlah=1;
            $result = mysqli_query($connect, "INSERT INTO cart (id_produk, Nama, nama_kategori, jumlah, qty, harga, gambar, id_cart ,id_pembeli) VALUES ('$id_produk', '$nama_produk', '$nama_kategori', '$jumlah', '$qty', '$harga', '$gambar', NULL, '$id_pembeli')");
        } else{
            $result =mysqli_query($connect, "UPDATE cart SET jumlah = jumlah + 1 WHERE id_produk='$id_produk'");
        }
    }

?>

<div class="container cart">
        <?php while ($index = mysqli_fetch_array($show_data_pembeli)) { ?>
            <?php echo $index['nama']; ?><br><?php echo $index['no_hp']; ?><br><?php echo $index['alamat']; ?>
        <?php } ?>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th class="stok">Stok</th>
                    <th>Harga</th>
                    <th class="harga">Total Harga</th>
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    while ($data = mysqli_fetch_array($sql)) { 
                        $no++;
                        $subtotal = $data['jumlah'] * $data['harga'];
                        $total = $total + $subtotal;
                ?>
                <form method="post" action="updatecart.php">
                    <tr>
                        <td>
                            <img src="image/gambar produk/<?php echo $data['gambar'] ?>" height="50" width="50">
                            
                        </td>
                        <td>
                            <?php echo $data['Nama']; ?> <br>
                            <p style="font-size: 0.8em;"><?php echo $data['nama_kategori']; ?>  </p>
                            <input type="hidden" name="id_cart" value="<?php echo $data['id_cart'] ?>">
                                    
                        </td>
                        <td>
                            <input type="number" min="1" value="<?php echo $data["jumlah"]; ?>" class="form-control" id="jumlah<?php echo $no; ?>" name="jumlah" >
                            <script>
                                $("#jumlah<?php echo $no; ?>").bind('change', function () {
                                    var jumlah<?php echo $no; ?>=$("#jumlah<?php echo $no; ?>").val();
                                    $("#jumlaha<?php echo $no; ?>").val(jumlah<?php echo $no; ?>);
                                });
                                $("#jumlah<?php echo $no; ?>").keydown(function(event) { 
                                    return false;
                                });
                                
                            </script>
                        </td>
                        <td class="isi-stok"><?php echo $data['qty']; ?></td>
                        <td>Rp. <?php echo number_format($data['harga'],0,',','.'); ?></td>
                        <td class="isi-harga">Rp.  <?php echo number_format($data['jumlah']*$data['harga'],0,',','.'); ?></td>
                        <td>
                            <input type="SUBMIT" name="btn_update" value="Update" class="btn btn-primary update">
                            <a href="hapuscart.php?id_cart=<?=$data['id_cart']?>" name="btn_hapus" value="hapus" class="btn btn-danger hapus">Hapus</a>
                        </td>
                    </tr>
                </form>
            <?php } ?>
            </tbody>
        </table>
        <h3 class="total">Total Bayar Rp. <?php echo number_format($total,0,',','.'); ?> </h3><br class="enter-cart">
        <a href="checkout.php" name="btn_checkout" class="btn btn-warning checkout">Checkout</a>
        <a href="home.php#Product" name="btn_belanja" class="btn btn-success belanja">Lanjut Belanja</a>
    </div>
